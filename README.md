# Terrarm | Provisioners

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition
Dans ce lab, nous allons mettre en oeuvre l'utilisation des modules dans un cas pratique.

Les modules sous Terraform sont des éléments essentiels permettant d'organiser et de réutiliser du code d'infrastructure. Ils regroupent des ressources Terraform liées par une fonctionnalité ou un objectif commun, offrant ainsi une abstraction et une modularité accrues. Les modules peuvent être composés de ressources, de variables, de sorties et d'autres éléments Terraform. Ils permettent de définir une interface claire et cohérente pour la configuration et le déploiement de l'infrastructure, ce qui facilite la gestion et la maintenance à grande échelle. Les modules favorisent également la réutilisation du code, réduisant ainsi la duplication et accélérant le processus de développement. En résumé, les modules Terraform sont des composants modulaires et réutilisables qui simplifient la gestion de l'infrastructure en permettant une organisation efficace et une abstraction cohérente des ressources.

## Contexte
Dans ce lab, il faudra :

1. Créer un module "ec2-module" afin de déployer une instance avec les ressources suivantes :
  - EC2
  - Security Group
  - EIP

2. Ensuite, créer deux dossiers, "prod" et "dev", chacun contenant un fichier Terraform (main.tf) utilisant le module "ec2-module" créé pour déployer une instance. Utiliser respectivement le type "t3.medium" pour la prod et "t2.micro" pour la dev.

3. Surcharger les tags de chaque environnement afin d'avoir respectivement "ec2-prod-<prefix>" pour la prod et "ec2-dev-<prefixe>" pour la dev.

4. Lancer la création des instances EC2 de dev et prod.

5. Une fois les instances provisionnées et Nginx fonctionnel, supprimer l'infrastructure.


## Création des modules

1. Crée un dossier **"modules"** à la racine du projet

2. Dans ce dossier on va créer un sous dossier nommé **"ec2-module"**

3. Créer dans le dossier **"ec2-module"** un fichier **"main.tf"**

4. Y copier le contenu du fichier **"ec2.tf"**, en prenant soin d'exclure les blocs de ressources contenant les données sensibles (access key, secret key)

5. Déplacer le fichier **"varibales.tf"** vers le dossier **"ec2-module"**

6. A la racine du projet, créer les dossier **"prod"** et **"dev"**

7. dans **"dev"** et **"prod"** créer respectivement un fichier **"main.tf"**

8. Une fois les **main.tf** créé, y copier les informations du provider et d'authenticiation

````bash
provider "aws" {
  region     = "us-east-1"
  access_key = "AKIASW5DVDMAVLHX2FAQ"
  secret_key = "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"
}

terraform {
  backend "s3" {
    bucket = "tf-backend-acd-1"
    key = "acd.tfstates"
    region = "us-east-1"
    access_key = "AKIASW5DVDMAVLHX2FAQ"
    secret_key = "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"
  }
}
````

Une fois toutes les informations du fichier ec2.tf récupérées, le supprimer.


9. Dans le bloc de connexion à l'instance ec2, bien renseigner le chemin vers la paire de clé ssh qui va permettre de s'authentifier et d'effectuer les configurations dans le serveur.
Le chemin ici est absolu, permettant de pointer sur l'emplacement du fichier peut importe son emplacement sur le disque.

````bash
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("C:/Users/Name_User/Directory/lab6-modules/secret/devops-aCD.pem")
    host = self.public_ip
    }
````

## Appel du module "ec2-module" dans les différents environnements
1. Dans le **"main.tf"** pour l'environnement **dev**, définir la source du module **"ec2-module"**
````bash
}

module "ec2" {
  source = "../modules/ec2-module"
}
````

2. Dans le **"main.tf"** de l'envoronnement **prod**, définir la source du module "ec2-module"
````bash
}

module "ec2" {
  source = "../modules/ec2-module"
}
````

## Surcharge des variables de chaque environnement "dev" et "prod"

1. Pour la **"dev"**, dans le bloc **module "ec2"** du fichier main.tf

y rajouter les informations de surcharge des variables **aws_common_tag**,  **instance_type** et **sg_name**

````bash
module "ec2" {
  source = "../modules/ec2-module"
  instance_type = "t2.micro"
  aws_common_tag = {
    Name = "ec2-dev-aCD"
  }
  sg_name = "dev-aCD-sg"
}
````



2. Pour **"prod"**, dans le bloc **moduel "ec2"** un fichier main.tf

y rajouter les informations de surcharge des variables **aws_common_tag**,  **instance_type** et **sg_name**
````bash
module "ec2" {
  source = "../modules/ec2-module"
  instance_type = "t3.medium"
  aws_common_tag = {
    Name = "ec2-prod-aCD"
  }
  sg_name = "prod-aCD-sg"
}
````

## Provisionnement de la dev

1. Initialisation du provider : terraform init
>![alt text](img/image.png)

2. Plannification de l'infra à provisionner : terraform plan
On peut constater que les variables **instance_type** et **aws_common_tag** ont bien été surchargées
>![alt text](img/image-2.png)
*Surchage des variables*

3. Provisionnement de l'infra de dev : terraform apply
>![alt text](img/image-3.png)
*Provissionnement de l'infra de dev réussi*

>![alt text](img/image-6.png)
*nginx est accessible sur le serveur de **dev***

## Provisionnement de la prod

1. initalisation du provider : terraform init
>![alt text](img/image-4.png)

2. Plannification de l'infra à provisionner : terraform plan
>![alt text](img/image-5.png)

3. Provisionnement de l'infra de prod : terraform apply
>![alt text](img/image-7.png)
*Provissionnement de l'infra de prod réussi*

>![alt text](img/image-8.png)
*nginx est accessible sur le serveur de **prod***

## Vue des instance dépuis la console AWS
>![alt text](img/image-9.png)
*Les instance de dev et prod en cours*

## Suppression de l'infra
on ferra un **`terraform destroy`** en se plançant au préalable dans le repertoire correspondant à chacun des environnements
1. Destroy prod
>![alt text](img/image-10.png)
*Suppression des ressources en prod*

2. Destroy dev
>![alt text](img/image-11.png)
*suppression des ressources en dev*
